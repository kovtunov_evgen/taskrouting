import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { HeroesComponent } from './heroes/heroes.component'
import { UserComponent } from './user/user.component'
import { HeroDetailComponent } from './hero-detail/hero-detail.component'
import { CulculatorComponent } from './calculator/calculator.component'

@NgModule({
  declarations: [
    AppComponent,
    HeroesComponent,
    UserComponent,
    HeroDetailComponent,
    CulculatorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
