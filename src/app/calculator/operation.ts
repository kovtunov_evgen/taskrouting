import {oper} from'./oper';

export const operations:oper[]=[
    {name:'+'},
    {name:'-'},
    {name:'*'},
    {name:'/'}
];