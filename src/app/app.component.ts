import { Component } from '@angular/core';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  imageBlocksArray: Array<ImageBlock>;

  constructor() {
    console.log("constructor");
  }

  ngOnInit(): void {

    var number = 12;
    var str = "dsadadada";

    this.imageBlocksArray = new Array<ImageBlock>();

    var image1 = new ImageBlock("https://naturalacousticguitar.com/wp-content/uploads/2017/01/wsi-imageoptim-Oasis_10169.jpg", "fake description", "Oasis");
    var image2 = new ImageBlock("https://i2-prod.mirror.co.uk/incoming/article5109931.ece/ALTERNATES/s615b/this-morning-kavana.jpg", "fake description2", "Kavana");

    var image3 = new ImageBlock("https://i.pinimg.com/736x/49/8e/4c/498e4c8c0d0fc95878ebf9288dde2c98--aries-altars.jpg", "fake description3", "Hui de Marcos");

    var image4 = new ImageBlock("https://www.aspentimes.com/wp-content/uploads/2017/08/jas-atd-020317-2.jpg", "fake description4", "Lake street");

    this.imageBlocksArray.push(image1);
    this.imageBlocksArray.push(image2);
    this.imageBlocksArray.push(image3);
    this.imageBlocksArray.push(image4);

  }


}

export class ImageBlock {
  constructor(path: string, descrp: string, name: string) {
    this.Description = descrp;
    this.Path = path;
    this.Name = name;
  }
  public Name: string
  public Path: string;
  public Description: string;
}